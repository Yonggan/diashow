# How to use it
First you need to crop your images to a width of 240px. Maybe use GIMP for that.
Note the height of the cropped image.

Place your image in the images folder and add your image in the `images.json` file.

Your entry should look like this:

```json
 {
    "fileName" : "Digitalcourage.png",
    "color": "#FFCC00",
    "ySize": 42,
    "backgroundColor": "#FFFFFF"
 }

```

`fileName`: Defines the filename in the images folder

`color`: Defines the base color of the leds

`ySize`: Defines the height in pixel of the image

`backgroundColor`: Defines the backgroundColor for use with transparent Images. Most time you can leave it to `#FFFFFF`
